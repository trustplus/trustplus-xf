<?php

class TrustPlus_Listener_LoadClassModel {
  public static function loadClassListener($class, &$extend) {
    if ($class == 'XenForo_Model_User')
      $extend[] = 'TrustPlus_Model_User';
    else if ($class == 'XenForo_Model_SpamPrevention')
      $extend[] = 'TrustPlus_Model_SpamPrevention';
  }
}
