<?php

class TrustPlus_Listener_LoadClassController {
  public static function loadClassListener($class, &$extend) {
    if ($class == 'XenForo_ControllerPublic_Register')
      $extend[] = 'TrustPlus_ControllerPublic_Register';
    else if ($class == 'XenForo_ControllerPublic_Account')
      $extend[] = 'TrustPlus_ControllerPublic_Account';
  }
}
