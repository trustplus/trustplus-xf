<?php

class TrustPlus_Model_User extends XFCP_TrustPlus_Model_User {
  public function ban($userID, $endDate, $reason, $update=false, &$errorKey=null, array $viewingUser=null, $triggered=false) {
    $r = parent::ban($userID, $endDate, $reason, $update, $errorKey, $viewingUser, $triggered);
    if (!$r)
      return $r;
    $options = XenForo_Application::get('options');

    if (!$options->TrustPlus_EnableBanReporting)
      return $r;

    try {
      $args = array();

      // get registration IP
      $regIP     = parent::getRegistrationIps($userID);
      if (is_array($regIP) && isset($regIP['register']))
        $args['reg-ip'] = $regIP['register'];

      // get last login IP
      $lastIP    = $this->_getDb()->fetchOne(
        'SELECT ip FROM xf_ip WHERE user_id = ? AND content_type=\'user\' AND action=\'login\' ORDER BY log_date DESC LIMIT 1', $userID);
      if ($lastIP)
        $args['last-ip'] = XenForo_Helper_Ip::convertIpBinaryToString($lastIP);

      // get e. mail, registration time
      $info    = $this->_getDb()->fetchRow(
        'SELECT email, register_date FROM xf_user WHERE user_id = ? LIMIT 1', $userID);
      $args['email']    = $info['email'];
      $regTime = new Zend_Date($info['register_date']);
      $args['reg-time'] = $regTime->getIso();

      if (is_string($reason) && strlen($reason) > 0)
        $args['summary'] = $reason;

      // report
      $tp_client = TrustPlus_Utils::create_client();
      $tp_client->reportBan($args);
    } catch (\Exception $e) {
      XenForo_Error::logException($e, true, 'Failed to report a ban to the Trust+ API. The following exception occurred: ');
    }

    return $r;
  }
}
