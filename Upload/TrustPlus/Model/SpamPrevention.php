<?php

class TrustPlus_Model_SpamPrevention extends XFCP_TrustPlus_Model_SpamPrevention {
  // Used to defer logging so we can correlate it with the user ID.
  public function logSpamTrigger($contentType, $contentID, $result=null, array $details=null, $userID=null, $ipAddr=NULL) {
    if (isset($GLOBALS['TrustPlus_DeferredLoggingInfo']) &&
        // This is how the registration controller calls us, check for exact pattern.
        $contentType === 'user' && is_int($contentID) && $result === null && $details === null) {
      $dl = $GLOBALS['TrustPlus_DeferredLoggingInfo'];
      $result = $dl['logResult'];
      $details = $dl['logDetails'];
      unset($GLOBALS['TrustPlus_DeferredLoggingInfo']);
    }

    return parent::logSpamTrigger($contentType, $contentID, $result, $details, $userID, $ipAddr);
  }
}
