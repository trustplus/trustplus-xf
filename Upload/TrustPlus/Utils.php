<?php

if (!class_exists('\TrustPlus\Client'))
  require_once(dirname(__FILE__) . '/Client.php');

class TrustPlus_Utils {
  static public function create_client() {
    $options = XenForo_Application::get('options');

    $api_key    = $options->TrustPlus_APIKey;
    $endpoint   = $options->TrustPlus_Endpoint;
    $ssl_verify = $options->TrustPlus_SSLVerify;

    $tp_client = new \TrustPlus\Client();
    $tp_client->setAPIKey($api_key);
    $tp_client->setEndpoint($endpoint);
    $tp_client->setVerifyPeer($ssl_verify);
    return $tp_client;
  }
}
