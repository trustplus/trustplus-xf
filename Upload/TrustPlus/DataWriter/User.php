<?php

class TrustPlus_DataWriter_User extends XFCP_TrustPlus_DataWriter_User {
  protected function _verifyEmail(&$email) {
    $ok = parent::_verifyEmail($email);
    if (!$ok)
      return false;

    // We don't want to stop the user's fields from being changed when it is being done e.g.
    // in the ACP. So we rather hackily use an Account controller hook to set a global flag
    // for the scope of this request which indicates that the user (and not an admin)
    // is trying to change their e. mail address.
    $options = XenForo_Application::get('options');

    if (!isset($GLOBALS['TrustPlus_UserChangingEmail']) || $email === $this->get('email') ||
        !$options->TrustPlus_EnableEmailChangeCheck)
      return true;

    // If there are already errors, don't bother checking.
    if ($this->getErrors())
      return true;

    $permit = true;
    $result = null;

    // call
    try {
      $tp_client = TrustPlus_Utils::create_client();

      $args = array(
        'email' => $email,
      );

      $result = $tp_client->check($args);

      if (is_array($result)) {
        if (isset($result['pass']) && $result['pass'] === false)
          $permit = false;

        if (isset($result['fail-open']) && is_array($result['fail-open'])) {
          if (isset($result['fail-open']['msg']))
            XenForo_Error::logError('Trust+ (e. mail change) call failed open: ' . $result['fail-open']['msg']);
          else
            XenForo_Error::logError('Trust+ (e. mail change) call failed open');
        }
      }
    } catch (\Exception $e) {
      // fail open
      XenForo_Error::logException($e, true, 'Failed to query the Trust+ API (e. mail change). The following exception occurred: ');
    }

    if (!$permit) {
      // The Account controller will send mail if it thinks the e. mail has been changed.
      //$this->_setInternal('xf_user', 'email', $this->getExisting('email'), true);
      //if ($this->isChanged('email'))
      //  throw new \Exception('still changed');

      $this->error(new XenForo_Phrase('trustplus_email_change_denied'), 'email', false);
      return false;
    }

    return true;
  }
}
