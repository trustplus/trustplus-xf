<?php

class TrustPlus_CAVHelper {
  static public function footer_modification_hook($hookName, &$contents,
      array $hookParams, XenForo_Template_Abstract $template) {
    $options = XenForo_Application::get('options');
    if (!$options->TrustPlus_EnableCAV)
      return;

    $jsDomain  = $options->TrustPlus_JSDomain;
    $apiKey    = $options->TrustPlus_APIKey;

    $keyInfo = XenForo_Application::getSimpleCacheData('TrustPlus_KeyInfo');
    if (!is_array($keyInfo) || $keyInfo['apiKey'] != $apiKey) {
      $tp_client = TrustPlus_Utils::create_client();
      $keyInfo = array(
        'apiKeyID' => $tp_client->getAPIKeyID(),
        'apiKey'   => $apiKey,
      );
      XenForo_Application::setSimpleCacheData('TrustPlus_KeyInfo', $keyInfo);
    }

    $apiKeyID = $keyInfo['apiKeyID'];

    $contents .= '<!-- BEGIN EMBEDDING CODE -->
<script src="//'.$jsDomain.'/ca/'.$apiKeyID.'/v1.js" defer="" async=""></script>
<iframe style="position:absolute;top:0;left:0;visibility:hidden;" src="//'.$jsDomain.'/ca/'.$apiKeyID.'/h1" id="tp-helper" sandbox="allow-same-origin allow-scripts allow-forms"></iframe>
<!-- END EMBEDDING CODE -->';
  }

  static public function register_modification_hook($templateName, &$content, array &$containerData, XenForo_Template_Abstract $template) {
    if ($templateName != 'register_form')
      return;

    // XXX - but unfortunately, there are no hooks for this (...)
    $content = str_replace('<select name="dob_month"', '<input type="hidden" name="cav_token" class="tp-token" value="" /><select name="dob_month"', $content);
  }
}
