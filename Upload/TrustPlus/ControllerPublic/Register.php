<?php

class TrustPlus_ControllerPublic_Register extends XFCP_TrustPlus_ControllerPublic_Register {
  protected function _runSpamCheck(XenForo_DataWriter_User $writer, array $extraErrors=array()) {
    $r = parent::_runSpamCheck($writer, $extraErrors);

    // don't bother to call the API if we have other errors
    if ($writer->getErrors() || count($extraErrors))
      return $r;

    // is enabled?
    $options = XenForo_Application::get('options');
    if (!$options->TrustPlus_EnableRegistrationCheck)
      return $r;

    $visitor = XenForo_Visitor::getInstance();
    $approvalThreshold = $options->TrustPlus_ManualApprovalThreshold;

    $email       = $writer->get('email');
    $ip          = XenForo_Helper_Ip::convertIpBinaryToString(
      XenForo_Helper_Ip::getBinaryIp($this->getRequest()));
    $user_agent  = '';
    if (isset($_SERVER['HTTP_USER_AGENT']))
      $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $cavToken    = $this->_input->filterSingle('cav_token', XenForo_Input::STRING);
    if (!is_string($cavToken))
      $cavToken = '';

    $permit = true;
    $result = null;
    $score = null;
    $tags = array();

    // call
    try {
      $tp_client = TrustPlus_Utils::create_client();

      $args = array(
        'email'      => $email,
        'ip'         => $ip,
        'user-agent' => $user_agent,
      );

      if ($options->TrustPlus_EnableCAV)
        $args['client-token'] = $cavToken;

      $result = $tp_client->check($args);

      if (is_array($result)) {
        if (isset($result['pass']) && $result['pass'] === false)
          $permit = false;

        if (isset($result['score']) && is_int($result['score']))
          $score = $result['score'];

        if (isset($result['tags']) && is_array($result['tags']))
          $tags = $result['tags'];

        if (isset($result['fail-open']) && is_array($result['fail-open'])) {
          if (isset($result['fail-open']['msg']))
            XenForo_Error::logError('Trust+ call failed open: ' . $result['fail-open']['msg']);
          else
            XenForo_Error::logError('Trust+ call failed open');
        }
      }
    } catch (\Exception $e) {
      // fail open
      XenForo_Error::logException($e, true, 'Failed to query the Trust+ API. The following exception occurred: ');
    }

    $logPhrase = $options->TrustPlus_LogApprovals ? 'trustplus_registration_allowed_log' : '';
    $logResult = XenForo_Model_SpamPrevention::RESULT_ALLOWED;
    if (!$permit) {
      $writer->error(new XenForo_Phrase('trustplus_registration_denied'));
      $logPhrase = 'trustplus_registration_denied_log';
      $logResult = XenForo_Model_SpamPrevention::RESULT_DENIED;
    } else if ($approvalThreshold >= 0 && is_int($score) && $score >= $approvalThreshold) {
      $writer->set('user_state', 'moderated');
      $logPhrase = 'trustplus_registration_moderated_log';
      $logResult = XenForo_Model_SpamPrevention::RESULT_MODERATED;
    }
    //else  // For debugging purposes, to avoid spamming the database with users.
    //  $writer->error(new XenForo_Phrase('trustplus_registration_denied_anyway',
    //    array('msg' => print_r($args, true), )));

    // Log the decision to the spam log.
    $tagsmsg = '';
    foreach ($tags as $k => $v) {
      $tagsmsg .= ', tag ' . $k . ' (' . $v['score'] . ')';
    }

    if ($logPhrase != '') {
      $logDetails = array(array(
        'phrase' => $logPhrase,
        'data' => array(
          'tags'       => $tagsmsg,
          'score'      => $score,
          'ip'         => $ip,
          'user_agent' => $user_agent,
        ),
        'result' => $result,
      ));

      // If we are moderating the user, then the user will be successfully
      // registered and the registration controller will call logSpamTrigger to
      // log the event. We want T+ information to show up in the user approval
      // page, which we can do if we attach the log information to that log
      // entry instead of generating our own. So when the user is moderated,
      // defer logging and intercept the call when it is made by the
      // registration controller.
      $logDeferred = ($logResult === XenForo_Model_SpamPrevention::RESULT_MODERATED);
      if (!$logDeferred) {
        $sp = $this->getModelFromCache('XenForo_Model_SpamPrevention');
        $sp->logSpamTrigger('user', null, $logResult, $logDetails);
      } else {
        $GLOBALS['TrustPlus_DeferredLoggingInfo'] = array(
          'logResult'  => $logResult,
          'logDetails' => $logDetails,
        );
      }
    }

    return $r;
  }
}
