Changelog
=========

1.0.13
------
- The extension requires `allow_url_fopen` to be set to on. From now on, an
  error is issued if this is not the case. This makes the cause of such issues
  clearer.

1.0.12
------
- When new registrations are moderated as a result of Trust+, the log entry showing
  the reasons why will now be shown on the user approval page as well as in the
  spam trigger log.

- Fix a bug causing an error where an HTTP request is sent without a User-Agent
  header.

1.0.11
------
- Merge changes from upstream trustplus-php client library, incorporating improved
  error reporting and resilience

1.0.10
------
- Fixed bug logging tags when a connection failure occurs.

1.0.9
-----
- E. mail addresses can now be checked when users attempt to change them.

1.0.8
-----
- Logging of successful registrations can now be disabled.

1.0.7
-----
- Now successful registrations and registrations marked for manual approval are logged.

1.0.6
-----
- Allow scores above a certain threshold value to be flagged for manual approval.

1.0.5
-----
- Incorporate trustplus-php fix for when PHP curlwrappers is used.

1.0.4
-----
- Now supports CAV.

1.0.3
-----
- Reports can now be made when a user is banned.

1.0.2
-----

- Disabled SSL verification by default, as there are some unresolved issues

1.0.1
-----

- Failed registrations are now logged in the Spam Trigger Log.
- Failure to call the Trust+ API is now logged in the Server Error Log.

1.0.0
-----

- Initial release.
