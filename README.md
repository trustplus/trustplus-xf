# Trust+ Integration Addon for XenForo

## Download the Extension

- [trustplus-xf.zip](https://bitbucket.org/trustplus/trustplus-xf/get/master.zip)

## Installation Instructions

1. Copy the `TrustPlus` directory into the XenForo `library` directory, so that you have `library/TrustPlus/...`.

2. Install the addon in the administration panel. Select the `TrustPlus/addon-trustplus.xml` file.

3. Go to "Trust+ Integration Options" in Home -> Options in the administration panel, and specify your API key.

## Support

See <https://trust.plus/>

## Licence

MIT License.
